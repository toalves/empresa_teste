# REST SERVICE / SWAPI

## CONFIGURA��O

######  EXECUTANDO FORA DO ECLIPSE

	H� o projeto j� compilado(JAR), assim permitindo a execu��o independente do Eclipse. Para tal execu��o, deve-se seguir uma estrutura de arquivos e diret�rios. Estes, localizados no diret�rio 'target/' 

	|-swapi-0.0.1-SNAPSHOT.jar
	|--conf/
	|--lib/


	Para executar:

	-Abrir o CMD(prompt de comando), digitar o comando abaixo
	-java -jar swapi-0.0.1-SNAPSHOT.jar


###### ARQUIVO DE PROPRIEDADES


	H� um arquivo de propriedades contido nesse projeto, cujo, deve ser configurado para habilitar a execu��o do mongoDB e Jersey. Tamb�m, permitindo o carregamento pela importa��o autom�tica dos planetas. Isso, caso o BD esteja vazio, e a configura��o habilitada.
	
	Exemplo:

	```
	resource.server.url=http://localhost:8089		
	resource.database.server.url=mongodb://127.0.0.1:27017
	resource.database.name=swapi
	import.if.empty=true
	number.of.planets=61	
	```

	**resource.server.url**
	Definindo a url, e porta, nas quais, o servidor REST vai rodar


	**resource.database.server.url**
	Define a url de conex�o ao mongoDB - N�o autenticar (sem suporte)

	**resource.database.name**
	Definindo o nome do banco de dados

	**import.if.empty**
	Definindo-se como true, e o banco de dados estando vazio, importar� os planetas do servi�o online SWAPI

	**number.of.planets**
	Quantidade de planetas � serem importados


###### BANCO DE DADOS MONGODB

	Utiliza-se a forma mais simples de opera��o do banco, sem autentica��o inclusive. O mongoDB j� deve estar em execu��o.

###### REST

	Est� sendo utilizada a implementa��o JERSEY do JAX-RS. N�o h� implementa��o de nenhum tipo de autentica��o ou quest�es de seguran�a. Sem header a mais est� sendo utilizado, apenas os m�todos padr�es GET, POST e DELETE.

###### POSTMAN

	H� um arquivo postman focado para testes nos servi�os, localizado no diretorio/package digital.b2w.swapi.util
	