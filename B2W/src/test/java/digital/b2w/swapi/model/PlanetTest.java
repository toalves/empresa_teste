package digital.b2w.swapi.model;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digital.b2w.swapi.config.PropertiesFactory;
import digital.b2w.swapi.config.ServerConnection;
import digital.b2w.swapi.service.PlanetServiceException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PlanetTest {

	private static final String URL_RESOURCE = PropertiesFactory.getInstance().getPropertieValue("resource.server.url");
	private static HttpServer server;
	private static Planet planetCreated;
	private static Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
	private static WebTarget target = client.target(URL_RESOURCE);
	
	@BeforeClass
	public static void startServer() throws IOException {
		server = ServerConnection.getInstance().getServer();
		server.start();
	}
	
	@AfterClass
	public static void shutdownServer() {
		server.shutdown();
	}
	
	@Test
	public void a_shouldNotSavePlanetWithoutName() throws PlanetServiceException {
		String planet = "{\r\n" +  
				"    \"climate\": \"Clima 8\",\r\n" + 
				"    \"terrain\": \"Terreno 8\"\r\n" + 
				"}";
		
		Entity<String> entity = Entity.entity(planet, MediaType.APPLICATION_JSON);
		Response response = target.path("/planets/new").request().post(entity);
		
		Assert.assertEquals(400, response.getStatus());		
	}
	
	@Test
	public void b_shouldNotSavePlanetWithoutClimate() throws PlanetServiceException {
		String planet = "{\r\n" + 
				"    \"name\": \"Alderaan\",\r\n" + 
				"    \"terrain\": \"Terreno 8\"\r\n" + 
				"}";
		
		
		Entity<String> entity = Entity.entity(planet, MediaType.APPLICATION_JSON);
		Response response = target.path("/planets/new").request().post(entity);
		
		Assert.assertEquals(400, response.getStatus());
		
	}
	
	@Test
	public void c_shouldNotSavePlanetWithoutTerrain() throws PlanetServiceException {
		String planet = "{\r\n" + 
				"    \"name\": \"Alderaan\",\r\n" + 
				"    \"climate\": \"Clima 8\"\r\n" + 
				"}";
		
		Entity<String> entity = Entity.entity(planet, MediaType.APPLICATION_JSON);
		Response response = target.path("/planets/new").request().post(entity);
		
		Assert.assertEquals(400, response.getStatus());
		
	}	
	
	@Test
	public void d_shouldReturnIdAfterSave() {
		String planet = "{\r\n" + 
				"    \"name\": \"Alderaan\",\r\n" + 
				"    \"climate\": \"Clima 10\",\r\n" + 
				"    \"terrain\": \"Terreno 10\"\r\n" + 
				"}";
		
		Entity<String> entity = Entity.entity(planet, MediaType.APPLICATION_JSON);
		Response response = target.path("/planets/new").request().post(entity);
		planetCreated = response.readEntity(Planet.class);
	
		Assert.assertEquals(201, response.getStatus());	
		Assert.assertTrue(planetCreated.getId() != null);
	}
	
	@Test	
	public void e_shouldFindPlanetEqualOfCreatedById() {		
		Planet planetFound = target.path("/planets/").path(planetCreated.getId()).request(MediaType.APPLICATION_JSON).get(Planet.class);
		Assert.assertEquals(planetFound.getId(), planetCreated.getId());
	}
	
	@Test	
	public void f_shouldFindPlanetEqualOfCreatedByNome() {		
		List<Planet> planets = target.path("/planets/name/").path(planetCreated.getName()).request(MediaType.APPLICATION_JSON).get(new GenericType<List<Planet>>() {} );
		Assert.assertTrue(planets.stream().filter(p -> planetCreated.getId().equals(p.getId())).findAny().orElse(null) != null);
	}	
	
	@Test
	public void g_shouldContainOneOrMorePlanets() {
		List<Planet> planets = target.path("/planets").request(MediaType.APPLICATION_JSON).get(new GenericType<List<Planet>>() {} );
		Assert.assertTrue(planets.size() > 0);
	}
	
	@Test
	public void h_shouldRemoveById() {
		Response response = target.path("/planets/remove").path(planetCreated.getId()).request().delete();
		Assert.assertEquals(202, response.getStatus());
	}
	

}
