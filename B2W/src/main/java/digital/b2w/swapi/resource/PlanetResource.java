package digital.b2w.swapi.resource;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.server.internal.scanning.ResourceFinderException;

import digital.b2w.swapi.model.Planet;
import digital.b2w.swapi.model.PlanetSearch;
import digital.b2w.swapi.service.PlanetService;
import digital.b2w.swapi.service.PlanetServiceException;

@Path("planets")
public class PlanetResource {
	
	private PlanetService service;
	
	public PlanetResource() {
		service = new PlanetService();
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() throws PlanetServiceException {
		 Planet[] planets = service.getAll().stream().toArray(Planet[]::new);
		 
		 if(planets == null || planets.length == 0) {
			 throw new ResourceFinderException("Resource not found");
		 }
		 
		 return Response.status(200).entity(planets).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") ObjectId id) throws PlanetServiceException {		 
				
		Planet planet = service.getById(id);
		
		if(planet == null) {
			throw new ResourceFinderException("Resource not found");
		}
		
		return Response.status(200).entity(planet).build();
	}		
	
	@GET
	@Path("name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByName(@PathParam("name") String name) throws PlanetServiceException {
		
		Planet[] planets = service.getByName(name).stream().toArray(Planet[]::new);
		
		if(planets == null || planets.length == 0) {
		  throw new ResourceFinderException("Resource not found");
		}		
		
		return Response.status(200).entity(planets).build();
	}	
	
	@POST
	@Path("new")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response add(Planet planet) throws PlanetServiceException {
		
		if(planet == null) {
			throw new BadRequestException("Planet was not informed");
		}
		
		if(planet == null || planet.getName() == null || planet.getName().isEmpty()) {
			throw new BadRequestException("Planet Name was not informed");
		}
		
		if(planet == null || planet.getClimate() == null || planet.getClimate().isEmpty()) {
			throw new BadRequestException("Planet Climate was not informed");
		}		
		
		if(planet == null || planet.getTerrain() == null || planet.getTerrain().isEmpty()) {
			throw new BadRequestException("Planet Terrain was not informed");
		}		
		
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
		WebTarget target = client.target("https://swapi.co/api");
		PlanetSearch planetSearch = target.path("/planets").queryParam("search", planet.getName()).request(MediaType.APPLICATION_JSON).get(PlanetSearch.class);
		
		if(planetSearch != null && planetSearch.getResults() != null) {
			if(planetSearch.getResults().size() == 1) {
				planet.setFilms(planetSearch.getResults().get(0).getFilms());
			}
		}
		
		service.save(planet);
		return Response.status(201).entity(planet).build();
	}
	
	@DELETE
	@Path("remove/{id}")
	public Response remove(@PathParam("id") ObjectId id) throws PlanetServiceException {
		Planet planet = service.getById(id);
		
		if(planet == null) {
			throw new ResourceFinderException("Resource not found");
		}
		
		service.remove(planet);
		return Response.status(202).type(MediaType.TEXT_PLAIN).entity("Planet removed successfully").build();
	}	
	
}
