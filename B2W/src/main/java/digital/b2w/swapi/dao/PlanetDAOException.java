package digital.b2w.swapi.dao;

@SuppressWarnings("serial")
public class PlanetDAOException extends Exception {

	public PlanetDAOException(String msg) {
		super(msg);
	}
	
	public PlanetDAOException(Throwable e) {
		super(e);
	}
	
	public PlanetDAOException(String msg, Throwable e) {
		super(msg, e);
	}
	
}
