package digital.b2w.swapi.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;

import digital.b2w.swapi.config.DBConnection;
import digital.b2w.swapi.model.Planet;

public class PlanetDAO {
	
	private Datastore dataStore = DBConnection.getInstance().getDataStore();
	
	public List<Planet> getAll() throws PlanetDAOException{
		try {
			return dataStore.createQuery(Planet.class).asList();
		}catch (Exception e) {
			throw new PlanetDAOException(e);
		}
	}
	
	public Planet getById(ObjectId id) throws PlanetDAOException {
		try {
			return (Planet) dataStore.createQuery(Planet.class).field("_id").equal(id).get();
		}catch (Exception e) {
			throw new PlanetDAOException(e);
		}
	}
	
	public List<Planet> getByName(String search) throws PlanetDAOException{
		try {
			return dataStore.createQuery(Planet.class).field("name").containsIgnoreCase(search).asList();
		}catch (Exception e) {
			throw new PlanetDAOException(e);
		}
	}
	
	public void save(Planet planet) throws PlanetDAOException {
		try {
			dataStore.save(planet);
		}catch (Exception e) {
			throw new PlanetDAOException(e);
		}
	}

	public void remove(Planet planet) throws PlanetDAOException {
		try {
			dataStore.delete(planet);
		}catch (Exception e) {
			throw new PlanetDAOException(e);
		}
	}
	
	public Long getCountAll() throws PlanetDAOException {
		try {
			return dataStore.getCount(Planet.class);
		}catch (Exception e) {
			throw new PlanetDAOException(e);
		}
	}
	
}
