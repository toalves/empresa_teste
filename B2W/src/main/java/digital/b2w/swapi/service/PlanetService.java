package digital.b2w.swapi.service;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.bson.types.ObjectId;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;

import digital.b2w.swapi.dao.PlanetDAO;
import digital.b2w.swapi.dao.PlanetDAOException;
import digital.b2w.swapi.model.Planet;

public class PlanetService {

	private PlanetDAO planetDAO;

	public PlanetService() {
		planetDAO = new PlanetDAO();
	}

	public List<Planet> getAll() throws PlanetServiceException {
		try {
			return planetDAO.getAll();
		} catch (PlanetDAOException e) {
			throw new PlanetServiceException(e);
		}
	}

	public Planet getById(ObjectId id) throws PlanetServiceException {
		try {
			return planetDAO.getById(id);
		}catch (Exception e) {
			throw new PlanetServiceException(e);
		}
	}

	public List<Planet> getByName(String search) throws PlanetServiceException {
		try {
			return planetDAO.getByName(search);
		} catch (PlanetDAOException e) {
			throw new PlanetServiceException(e);
		}
	}
	
	public void save(Planet planet) throws PlanetServiceException {
		try {
			planetDAO.save(planet);
		} catch (PlanetDAOException e) {
			throw new PlanetServiceException(e);
		}
	}
	
	public void remove(Planet planet) throws PlanetServiceException {
		try {
			planetDAO.remove(planet);
		} catch (PlanetDAOException e) {
			throw new PlanetServiceException(e);
		}
	}
	
	public Planet insertPlanetFromOnLineAPI(Integer id) throws PlanetServiceException {
		try {
			
			Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);
			WebTarget target = client.target("https://swapi.co/api");
			Planet planet = new Planet();
						
			planet = target.path("/planets/").path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).get(Planet.class);
			
			if(planet != null) {
				planetDAO.save(planet);
			}
			
			return planet;
		}catch (Exception e) {
			throw new PlanetServiceException(e);
		}
	}
	
	public Long getCountAll() throws PlanetServiceException{
		try {
			return planetDAO.getCountAll();
		}catch (Exception e) {
			throw new PlanetServiceException(e);
		}
	}

}
