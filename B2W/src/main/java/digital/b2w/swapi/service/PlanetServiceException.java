package digital.b2w.swapi.service;

@SuppressWarnings("serial")
public class PlanetServiceException extends Exception {

	public PlanetServiceException(String msg) {
		super(msg);
	}
	
	public PlanetServiceException(Throwable e) {
		super(e);
	}
	
	public PlanetServiceException(String msg, Throwable e) {
		super(msg, e);
	}	
	
}
