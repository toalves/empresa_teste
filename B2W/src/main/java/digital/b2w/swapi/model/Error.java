package digital.b2w.swapi.model;

public class Error {

	private String description;
	private Integer statusCode;
	
	public Error() {
	}

	public Error(String description) {
		this.description = description;
	}

	public Error(String description, Integer statusCode) {
		this.description = description;
		this.statusCode = statusCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

}
