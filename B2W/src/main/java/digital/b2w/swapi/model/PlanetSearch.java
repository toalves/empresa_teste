package digital.b2w.swapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanetSearch {

	private Integer count;
	private List<Planet> results;
	
	public PlanetSearch() {
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<Planet> getResults() {
		return results;
	}

	public void setResults(List<Planet> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "PlanetSearch [count=" + count + ", results=" + results + "]";
	}

}
