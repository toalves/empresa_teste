package digital.b2w.swapi.config;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class DBConnection {

	private static DBConnection instance;
	private static Datastore dataStore;
	
	private DBConnection() {
		MongoClient mongoClient = new MongoClient(new MongoClientURI(PropertiesFactory.getInstance().getPropertieValue("resource.database.server.url")));
		dataStore = new Morphia().createDatastore(mongoClient, PropertiesFactory.getInstance().getPropertieValue("resource.database.name"));
	}
	
	public static DBConnection getInstance() {
		if(instance == null) {
			instance = new DBConnection();
		}
		
		return instance;
	}
	
	public Datastore getDataStore() {
		return dataStore;
	}
}
