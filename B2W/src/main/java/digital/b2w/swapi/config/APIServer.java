package digital.b2w.swapi.config;

import org.glassfish.grizzly.http.server.HttpServer;

import digital.b2w.swapi.model.Planet;
import digital.b2w.swapi.service.PlanetService;

public class APIServer {

	public static void main(String[] args) {
		try {
			
			System.out.println("[##### INICIANDO SERVIDOR #####]");
			
			HttpServer server = ServerConnection.getInstance().getServer();
			server.start();
			
			PlanetService planetService = new PlanetService();
			
			if(Boolean.parseBoolean(PropertiesFactory.getInstance().getPropertieValue("import.if.empty")) && planetService.getCountAll() == 0) {
				System.out.println("[##### CONFIGURADO PARA IMPORTAR SE O BD ESTIVER VAZIO #####]");
				try {
					for(int i=1; i<=Integer.parseInt(PropertiesFactory.getInstance().getPropertieValue("number.of.planets")); i++) {
						Planet planet = planetService.insertPlanetFromOnLineAPI(i);
						System.out.println("[##### PLANETA [" + planet.getName() + "] INSERIDO COM SUCESSO #####]");
					}
					
					System.out.println("[##### IMPORTAÇÃO CONCLUÍDA #####]");
				}catch (Exception e) {
					System.out.println("[##### FALHA AO PROCESSAR A IMPORTAÇÃO ==> " + e.getMessage() + " #####]");
				}
				
			}
			
			System.out.println("[##### SERVIDOR INICIADO - PRESSIONE QUALQUER TECLA PARA FINALIZAR ... #####]");
			
			System.in.read();
			server.shutdown();
			System.out.println("[##### SERVIDOR FINALIZADO - PRESSIONE QUALQUER TECLA PARA SAIR ... #####]");
			
			System.in.read();
			
		} catch (Exception e) {
			System.out.println(e);
		}
				
	}

}
