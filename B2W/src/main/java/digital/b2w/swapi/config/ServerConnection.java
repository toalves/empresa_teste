package digital.b2w.swapi.config;

import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import digital.b2w.swapi.resource.PlanetResource;

public class ServerConnection {

	private static ServerConnection instance;
	private static HttpServer server;
	
	private ServerConnection() {
		URI uri = URI.create(PropertiesFactory.getInstance().getPropertieValue("resource.server.url"));
		ResourceConfig config = new ResourceConfig().packages("digital.b2w.swapi");
		config.register(JacksonFeature.class);
		config.registerClasses(PlanetResource.class, ResourcesExceptionMapper.class);
		
		server = GrizzlyHttpServerFactory.createHttpServer(uri, config);
	}
	
	public static ServerConnection getInstance() {
		if(instance == null) {
			instance = new ServerConnection();			
		}
		
		return instance;
	}
	
	public HttpServer getServer() {
		return server;
	}
	
}
