package digital.b2w.swapi.config;

import java.util.Properties;

public class PropertiesFactory {

	private static PropertiesFactory instance;
	private static Properties prop;
	
	private PropertiesFactory() {
		try {
			prop = new Properties();
			prop.load(APIServer.class.getResourceAsStream("/swapi.properties"));
		}catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static PropertiesFactory getInstance() {
		if(instance == null) {
			instance = new PropertiesFactory();
		}
		
		return instance;
	}
	
	public String getPropertieValue(String key) {
		return prop.getProperty(key);
	}
	
	
	
}
