package digital.b2w.swapi.config;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.glassfish.jersey.server.internal.scanning.ResourceFinderException;

import digital.b2w.swapi.model.Error;

public class ResourcesExceptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable t) {
		
		Error error = new Error(t.getMessage(), 500);
		
		if(t instanceof ResourceFinderException) {
			error.setStatusCode(404);
		}else if(t instanceof BadRequestException) {
			error.setStatusCode(400);
		}
		
		
		return Response.status(error.getStatusCode()).entity(error).type(MediaType.APPLICATION_JSON).build();
	}

}
